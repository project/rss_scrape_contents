# RSS Feed Scraper Content

## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

Project to help convert RSS Feeds into node contents inside Drupal 8.

## REQUIREMENTS

No special requirements.

## INSTALLATION

- Install as you would normally install a contributed Drupal project. Visit
  documentation for further information.

## CONFIGURATION

- Under development.

## MAINTAINERS

### Current maintainers:

- Vitor Grillo (vitorgrillo) - https://www.drupal.org/user/3436121
